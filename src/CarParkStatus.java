import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ThreadPoolExecutor;

public class CarParkStatus {

    //there should never be more than one CarParkStatus
    private static CarParkStatus instance;

    private CarParkStatus() {
    }

    public static CarParkStatus getInstance() {
        if (CarParkStatus.instance == null) {
            CarParkStatus.instance = new CarParkStatus();
        }
        return CarParkStatus.instance;
    }

//Parameters


    private SpotType standardSpots = SpotType.getInstance();
    private SpotType handicappedSpots = SpotType.getInstance();
    private SpotType womenSpots = SpotType.getInstance();
    private SpotType pkwSpots = SpotType.getInstance();
    private SpotType pickUpSpots = SpotType.getInstance();
    private SpotType suvSpots = SpotType.getInstance();
    private SpotType bikeSpots = SpotType.getInstance();
    private SpotType trikeSpots = SpotType.getInstance();
    private SpotType quadSpots = SpotType.getInstance();


    //Setters
    public void setStandardSpots(ParkingSpot[] standardSpots) {
        this.standardSpots.setParkingSpots(standardSpots);
    }

    public void setHandicappedSpots(ParkingSpot[] handicappedSpots) {
        this.handicappedSpots.setParkingSpots(handicappedSpots);
    }

    public void setWomenSpots(ParkingSpot[] womenSpots) {
        this.womenSpots.setParkingSpots(womenSpots);
    }

    public void setBikeSpots(ParkingSpot[] bikeSpots) {
        this.bikeSpots.setParkingSpots(bikeSpots);
    }


    public void setpkwSpots(ParkingSpot[] pkwSpots) {
        this.pkwSpots.setParkingSpots(pkwSpots);
    }


    public void setPickUpSpots(ParkingSpot[] pickUpSpots) {
        this.pickUpSpots.setParkingSpots(pickUpSpots);
    }


    public void setSuvSpots(ParkingSpot[] suvSpots) {
        this.suvSpots.setParkingSpots(suvSpots);
    }


    public void setTrikeSpots(ParkingSpot[] trikeSpots) {
        this.trikeSpots.setParkingSpots(trikeSpots);
    }


    public void setQuadSpots(ParkingSpot[] quadSpots) {
        this.quadSpots.setParkingSpots(quadSpots);
    }


    //Getters
    public int getFreeStandardSpots() {
        return standardSpots.getfreeSpots();
    }

    public int getFreeHandicappedSpots() {
        return handicappedSpots.getfreeSpots();

    }

    public int getFreeBikeSpots() {

        return bikeSpots.getfreeSpots();

    }

    public int getFreeWomenSpots() {
        return womenSpots.getfreeSpots();

    }

    public int getFreePkwSpots() {

        return pkwSpots.getfreeSpots();

    }

    public int getFreeSuvSpots() {

        return suvSpots.getfreeSpots();

    }

    public int getFreePickUpSpots() {

        return pickUpSpots.getfreeSpots();

    }

    public int getFreeTrikeSpots() {

        return trikeSpots.getfreeSpots();

    }

    public int getFreeQuadSpots() {

        return  quadSpots.getfreeSpots();

    }

    public SpotType getStandardSpots() {
        return standardSpots;
    }

    public SpotType getHandicappedSpots() {
        return handicappedSpots;
    }

    public SpotType getWomenSpots() {
        return womenSpots;
    }

    public SpotType getBikeSpots() {
        return bikeSpots;
    }

    public SpotType getPkwSpots() {
        return pkwSpots;
    }

    public SpotType getSuvSpots() {
        return suvSpots;
    }

    public SpotType getPickUpSpots() {
        return pickUpSpots;
    }

    public SpotType getTrikeSpots() {
        return trikeSpots;
    }

    public SpotType getQuadSpots() {
        return quadSpots;
    }

    //Members
//occupy-Methods
    public String occupyStandardSpot() {

        return standardSpots.occupySpot(standardSpots.getSpots());
    }

    public String occupyHandicappedSpot() {

        return handicappedSpots.occupySpot(handicappedSpots.getSpots());
    }

    public String occupyWomenSpot() {

        return womenSpots.occupySpot(womenSpots.getSpots());
    }

    public String occupyBikeSpot() {

        return bikeSpots.occupySpot(bikeSpots.getSpots());
    }

    public String occupyPkwSpot() {

        return pkwSpots.occupySpot(pkwSpots.getSpots());
    }

    public String occupySuvSpot() {

        return suvSpots.occupySpot(suvSpots.getSpots());
    }

    public String occupyPickUpSpot() {

        return pickUpSpots.occupySpot(pickUpSpots.getSpots());
    }


    public String occupyTrikeSpot() {

        return trikeSpots.occupySpot(trikeSpots.getSpots());
    }

    public String occupyQuadSpot() {

        return quadSpots.occupySpot(quadSpots.getSpots());
    }

    public double leaveSpot(String number) {
        double res = standardSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = handicappedSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = womenSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = pkwSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = suvSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = bikeSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = trikeSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = quadSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        res = pickUpSpots.leaveSpot(number);
        if (res != -1) {
            return res;
        }
        throw new NoSuchElementException();
    }
}


