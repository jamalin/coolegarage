import java.util.ArrayList;
import java.util.List;

public class SpotType {      //funktioniert wie das Singleton, Liste von Singletons

    private static final int NUM_OF_INSTANCES = 9;
    private static final List<SpotType> instanceList = new ArrayList<SpotType>();
    private static int instanceCount = 0;   //immer wenn getInstance aufgerufen wird wird der Counter einen hochgezählt

    private  ParkingSpot[] spots = new ParkingSpot[0];
    private  double parkingArea;
    private  double pricePerHour;
    private  int freeSpots = 0;

    private SpotType()
    {

    }

    public static SpotType getInstance(){
        SpotType instance;

        if(instanceList.size() == NUM_OF_INSTANCES){
            instance = instanceList.get(instanceCount%NUM_OF_INSTANCES);
        }else {
            instance = new SpotType();
            instanceList.add(instance);
        }
        instanceCount++;
        return instance;
    }

    public void init(ParkingSpot[] numberOfSpots, double parkingArea, double pricePerHour ){
        this.spots = numberOfSpots;
        this.parkingArea = parkingArea;
        this.pricePerHour = pricePerHour;
        this.freeSpots = spots.length;
    }

    public ParkingSpot[] getSpots(){
        return spots;
    }
    public double getparkingArea(){
        return parkingArea;
    }
    public double getPricePerHour(){
        return pricePerHour;
    }

    public int getfreeSpots(){
        return freeSpots;
    }


    public void setParkingSpots(ParkingSpot[] spots){
        this.spots = spots;
    }


    public String occupySpot(ParkingSpot[] spots) {
        for (ParkingSpot spot : spots) {
            if (!spot.isOccupied()) {
                freeSpots--;
                spot.occupy();
                return spot.getNumber();
            }
        }

        throw new RuntimeException("No empty Spot of this type found");
    }


    public double leaveSpot(String number){

        for(int i = 0; i < spots.length; i++){
            if(number.equals(spots[i].getNumber())){
                freeSpots++;
                return pricePerHour * spots[i].leave();

            }
        }
        return -1;
    }
}


