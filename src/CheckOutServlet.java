import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;

@WebServlet(name = "CheckOutServlet")
public class CheckOutServlet extends HttpServlet {

    private CarParkStatus carParkStatus;
    private boolean error = false;

    public CheckOutServlet() {
        carParkStatus = CarParkStatus.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String input = request.getParameter("number");
        String formattedPrice = "";

        try {
            formattedPrice = String.format("%.2f", carParkStatus.leaveSpot(input));
        } catch (NoSuchElementException e) {
            error = true;
            doGet(request, response);
            return;
        }

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        WebWriter writer = new WebWriter(response.getWriter());

        String title = "Parkhaus verlassen";
        String[] body = {
                "<h1> Vielen Dank fuer Ihren Besuch bei CooleGarage! Ihre Rechnung betraegt " + formattedPrice + "€ </h1><br>",
                "<a href=\"CheckOutServlet\" class=\"button no-block\"> Okay </a>"
        };
        writer.write(title, body);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String errorMessage = "";
        if (error)  {
            errorMessage = "<span class=\"error\">Ihre angegebene Parkplatznummer ist nicht vergeben.</span>";
            error = false;
        }
        String[] body = {
                errorMessage,
                "<form action = \"CheckOutServlet\" method = \"POST\">",
                "Bitte Nummer eingeben: <input type=\"text\" name=\"number\"><br>",
                "<input type=\"submit\" value=\"Bestaetigen\" class=\"button no-block\">",
                "<form>"
        };
        WebWriter writer = new WebWriter(response.getWriter());
        writer.write("Bitte Parplatznummer eingeben", body);
    }
}
