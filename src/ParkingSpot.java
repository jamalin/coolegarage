import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class ParkingSpot {

    private LocalDateTime occupiedFrom;
    private String level;
    private String number;

    public ParkingSpot(int level, int number) {
        this.level = String.format("%02d", level);
        this.number = String.format("%03d", number);
    }

    public String occupy() {
        occupiedFrom = LocalDateTime.now();
        return getNumber();
    }

    public String getNumber() {
        return level + "." + number;
    }

    public boolean isOccupied() {
        return !(occupiedFrom == null);
    }

    public double leave() {
        double time = Duration.between(occupiedFrom, LocalDateTime.now()).getSeconds();
        occupiedFrom = null;
        return time / (60 * 60);
    }

    public String getOccupiedSince() {
        return occupiedFrom.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

}
